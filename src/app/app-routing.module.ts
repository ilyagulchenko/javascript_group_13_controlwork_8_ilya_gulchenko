import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeQuotesComponent} from './home-quotes/home-quotes.component';
import {NewQuoteComponent} from './new-quote/new-quote.component';
import {ManageQuotesComponent} from './manage-quotes/manage-quotes.component';
import {QuoteDetailsComponent} from './manage-quotes/quote-details/quote-details.component';

const routes: Routes = [
  {path: 'add-quote', component: NewQuoteComponent},
  {path: '', component: ManageQuotesComponent, children: [
      {path: ':category', component: QuoteDetailsComponent},
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
