import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Quote} from '../../shared/quote.model';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.component.html',
  styleUrls: ['./quote-details.component.css']
})
export class QuoteDetailsComponent implements OnInit {
  quotes!: Quote[];

  constructor(private http: HttpClient, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.http.get<{[id: string]: Quote}>('https://plovo-cc061-default-rtdb.firebaseio.com/quotes.json').subscribe(result => {
      this.quotes = Object.keys(result).map(id => {
        const quoteData = result[id];

        return new Quote(
          id,
          quoteData.category,
          quoteData.author,
          quoteData.text,
        );
      });
    });
  }

}
