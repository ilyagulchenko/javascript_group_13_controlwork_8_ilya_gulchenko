import { Component, OnInit } from '@angular/core';
import {Quote} from '../shared/quote.model';

@Component({
  selector: 'app-manage-quotes',
  templateUrl: './manage-quotes.component.html',
  styleUrls: ['./manage-quotes.component.css']
})
export class ManageQuotesComponent implements OnInit {
  quote!: Quote;

  constructor() { }

  ngOnInit() {

  }

}
