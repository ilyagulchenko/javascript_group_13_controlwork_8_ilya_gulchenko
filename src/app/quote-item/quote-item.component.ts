import {Component, Input} from '@angular/core';
import {Quote} from '../shared/quote.model';

@Component({
  selector: 'app-quote-item',
  templateUrl: './quote-item.component.html',
  styleUrls: ['./quote-item.component.css']
})
export class QuoteItemComponent {
  @Input() quote!: Quote;

  getCategory(category: string) {
    if (category === 'star-wars') {
      return 'Star Wars';
    } if (category === 'humour') {
      return 'Humour';
    } if (category === 'football') {
      return 'Football';
    } if (category === 'famous-people') {
      return 'Famous people';
    } else return 'Other';
}
}
