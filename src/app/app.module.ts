import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { HomeQuotesComponent } from './home-quotes/home-quotes.component';
import { ManageQuotesComponent } from './manage-quotes/manage-quotes.component';
import { QuoteDetailsComponent } from './manage-quotes/quote-details/quote-details.component';
import {HttpClientModule} from '@angular/common/http';
import { QuoteItemComponent } from './quote-item/quote-item.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewQuoteComponent,
    HomeQuotesComponent,
    ManageQuotesComponent,
    QuoteDetailsComponent,
    QuoteItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
