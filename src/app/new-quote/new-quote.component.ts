import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent {
  category = '';
  author = '';
  text = '';

  constructor(private http: HttpClient) { }

  addQuote() {
    const category: string = this.category;
    const author: string = this.author;
    const text: string = this.text;

    const body = {category, author, text};
    this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/quotes.json', body).subscribe();
  }
}
